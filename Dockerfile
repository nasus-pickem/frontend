FROM node:21-alpine3.18 as build
WORKDIR /app

COPY src/package.json .
COPY src/package-lock.json .
RUN npm install
COPY src/ .
RUN npm run build --omit=dev

FROM nginx:1.25.2-alpine3.18 as runtime
COPY --from=build /app/dist/nasus-pickem/browser /usr/share/nginx/html
